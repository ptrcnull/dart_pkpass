## v2.0.1

- fix: do not match scale if localization empty (The one with the braid)
- fix: use broader constraint for intl (The one with the braid)
- fix: version in CHANGELOG (The one with the braid)

## v2.0.0

- refactor: use binary barcode data by default (The one with the braid)

## v1.3.0

- chore: add funding (The one with the braid)
- chore: link demo project in README (The one with the braid)
- feat: implement web service (The one with the braid)

## v1.2.2

- fix: remove print statements (The one with the braid)

## v1.2.1

- fix: invalid JSON parser for eventTicket (The one with the braid)

## v1.2.0

- chore: support complete CSS color codes (The one with the braid)

## v1.1.2

- fix: code style (The one with the braid)

## v1.1.1

- chore: export more public classes (The one with the braid)

## v1.1.0

- chore: add high level classes (The one with the braid)
- chore: bump version (The one with the braid)
- feat: add String localization support (The one with the braid)

## v1.0.0

- chore: initial commit (The one with the braid)

