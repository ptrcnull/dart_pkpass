# pkpass

A Dart native pkpass parsing library.

See a demo ? Check out [fWallet web](https://theonewiththebraid.gitlab.io/f_wallet/)
[[link to repo](https://gitlab.com/TheOneWithTheBraid/f_wallet)] !

## Features

- no platform specific dependencies - pure Dart
- parse any .pkpass file as blob
- checksum verification
- extract metadata
- high level lookup for assets by locale and scale, with proper fallbacks
- high level barcode API
- No dependency on `dart:io` or `dart:ui` runs everywhere, from CLI to Flutter

## Not supported (yet)

Some parts of the PkPass specification are either not yet implemented, or not planned, such as:

- `signature`: The detached PKCS #7 signature using Apple certificates of the manifest. Note: Checksums _are_ checked. -
  Not planned, feel free to contribute.
- `nfc`: Card payment information for Apple Pay. - Not planned, feel free to contribute.
- `webService`: Only pull to refresh supported. Push service not implemented yet. - Planned, feel free to contribute.

## Localizations

This package aims to implement PkPass localizations as well as possible. Any localizable value
can be accessed using a `getLocalized...` method, e.g. `myPass.getLocalizedDescription()` taking a `Locale`
as argument. In case the requested locale is not available, the following fallbacks are used:

- `en` - English, any
- `zh` - Chinese, generic language group
- `hi` - Hindi
- `es` - Spanish
- `fr` - French
- *In case neither available, take just any language you can find. We are likely dealing with a local product then.*

The used fallback languages are the five mostly understood languages in the world, feel free to propose better or more
precise fallback mechanisms.

## Barcode encodings

The PkPass standard is quite vague about the Barcode String encoding used. Technically, all IANA character set names
are allowed. Since this might be some overhead to implement, the following encoders are supported by default:

- `Latin1Codec` (default according to PkPass spec) - `iso-8859-1`, also fallback onto `iso-8859` and `iso8859`
- `Utf8Codec` (most common one) - `utf-8`, also fallback onto `utf8`

The supported encoders can be extended by adding a `String` `Encoder` pair to `PassBarcode.supportedCodecs`.

## Dependencies and compatibility

Any package should keep its dependencies as minimal as possible. Sometimes, there are specifications making this
difficult. The PkPass spec unfortunately is a very complex one, requiring support of many standards and formats.

The following dependencies are used to correctly parse the PkPass file into a relevant Dart representation.

- [`pub:archive`](https://pub.dev/packages/archive): The PkPass file itself is a ZIP archive, used to parse the raw
  bytes.
- [`pub:barcode`](https://pub.dev/packages/barcode): Used to provide high-level access to barcode generation with the
  proper encoding supported.
- [`pub:crypto`](https://pub.dev/packages/crypto): Used for SHA1 signature verification as defined in the PkPass spec.
- [`pub:intl`](https://pub.dev/packages/intl): Used for localization lookup of localizable resources like Strings or
  assets.

## Getting started

```dart
import 'dart:io';

import 'package:intl/locale.dart';
import 'package:pkpass/pkpass.dart';

Future<int> main(List<String> args) async {
  print('Using first argument or stdin file name: ');
  final path = args.singleOrNull ?? stdin.readLineSync();
  if (path == null || path.isEmpty) {
    print('Please enter a file name or provide it as single argument.');
    return 1;
  }

  final file = File(path);
  final contents = await file.readAsBytes();

  final pass = await PassFile.parse(contents);

  final logo = pass.getLogo(
    scale: 2,
    locale: Locale.fromSubtags(languageCode: 'fr'),
  );

  print('Logo image blob length: ${logo?.length}');

  return 0;
}

```

## Additional information

Like this project? [Buy me a Coffee](https://www.buymeacoffee.com/braid).

License : EUPL-1.2
