import 'dart:convert';
import 'dart:typed_data';

import 'package:barcode/barcode.dart';

/// Information about a pass’s barcode.
///
/// Use with Flutter:
///
/// ```dart
/// BarcodeWidget.fromBytes(
///   barcode: Barcode.fromType(myPassBarcode.format),
///   data: myPassBarcode.barcodeData,
/// )
/// ```
///
/// Please not the spec requires you to display the [altText] next to the
/// barcode in case available.
class PassBarcode {
  /// the [Encoding] supported for [messageEncoding]. Can be expanded at runtime.
  ///
  /// Default values are
  /// - iso-8859-1: [Latin1Codec]
  /// - utf-8: [Utf8Codec]
  static Map<String, Encoding> supportedCodecs = {
    'iso-8859-1': latin1,
    'iso-8859': latin1,
    'iso8859': latin1,
    'utf-8': utf8,
    'utf8': utf8,
  };

  static const _allowedFormats = {
    'PKBarcodeFormatQR': BarcodeType.QrCode,
    'PKBarcodeFormatPDF417': BarcodeType.PDF417,
    'PKBarcodeFormatAztec': BarcodeType.Aztec,
    'PKBarcodeFormatCode128': BarcodeType.Code128,
  };

  /// Barcode format. For the barcode dictionary, you can use only the
  /// following values: PKBarcodeFormatQR, PKBarcodeFormatPDF417, or
  /// PKBarcodeFormatAztec. For dictionaries in the barcodes array, you may
  /// also use PKBarcodeFormatCode128.
  ///
  /// The spec defined keys are converted into the corresponding [BarcodeType]
  /// representations.
  final BarcodeType format;

  /// Correctly encoded byte list to be displayed in the [barcode].
  final Uint8List barcodeData;

  /// Text encoding that is used to convert the message from the string
  /// representation to a data representation to render the barcode.
  ///
  /// The value is typically iso-8859-1, but you may use another encoding that
  /// is supported by your barcode scanning infrastructure.
  ///
  /// Only supported values by this packages are:
  /// - iso-8859-1
  /// - utf-8
  ///
  /// Custom codecs can be provided as [Codec] in [PassBarcode.supportedCodecs].
  final Encoding messageEncoding;

  /// Text displayed near the barcode. For example, a human-readable version
  /// of the barcode data in case the barcode doesn’t scan.
  final String? altText;

  PassBarcode({
    required this.format,
    @Deprecated('Use [barcodeData] instead') String? message,
    this.messageEncoding = utf8,
    Uint8List? barcodeData,
    required this.altText,
  }) : barcodeData =
            Uint8List.fromList(barcodeData ?? messageEncoding.encode(message!));

  factory PassBarcode.fromJson(Map<String, Object?> json) {
    final messageEncoding =
        supportedCodecs[(json['messageEncoding'] as String).toLowerCase()]!;
    return PassBarcode(
      format: _allowedFormats[json['format']]!,
      barcodeData:
          Uint8List.fromList(messageEncoding.encode(json['message'] as String)),
      messageEncoding: messageEncoding,
      altText: json['altText'] as String?,
    );
  }

  /// Message or payload to be displayed as a barcode.
  ///
  /// Do not use directly, use the encoded [barcodeData] instead.
  String get message => messageEncoding.decode(barcodeData);
}
