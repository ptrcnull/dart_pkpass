/// Information about a location.
class Location {
  /// Latitude, in degrees, of the location.
  final double latitude;

  /// Longitude, in degrees, of the location.
  final double longitude;

  /// Altitude, in meters, of the location.
  final double? altitude;

  /// Text displayed on the lock screen when the pass is currently relevant.
  /// For example, a description of the nearby location such as
  /// “Store nearby on 1st and Main.”
  final String? relevantText;

  const Location({
    required this.latitude,
    required this.longitude,
    this.altitude,
    this.relevantText,
  });

  factory Location.fromJson(Map<String, Object?> json) => Location(
        latitude: json['latitude'] as double,
        longitude: json['longitude'] as double,
        altitude: json['altitude'] as double?,
        relevantText: json['relevantText'] as String?,
      );
}
