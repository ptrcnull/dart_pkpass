/// Metadata required for Pass Web Service
///
/// https://developer.apple.com/library/archive/documentation/PassKit/Reference/PassKit_WebService/WebService.html#//apple_ref/doc/uid/TP40011988
class PassWebService {
  /// The authentication token to use with the web service.
  /// The token must be 16 characters or longer.
  final String authenticationToken;

  /// The URL of a web service that conforms to the API described in PassKit Web Service Reference.
  final Uri webServiceURL;

  const PassWebService({
    required this.authenticationToken,
    required this.webServiceURL,
  });

  /// returns a [PassWebService] in case [authenticationToken] and
  /// [webServiceURL] are both valid values.
  static PassWebService? maybe({
    String? authenticationToken,
    String? webServiceURL,
  }) {
    if (authenticationToken == null || webServiceURL == null) return null;
    final uri = Uri.tryParse(webServiceURL);
    if (uri == null || uri.scheme != 'https') return null;
    return PassWebService(
      authenticationToken: authenticationToken,
      webServiceURL: uri,
    );
  }
}
