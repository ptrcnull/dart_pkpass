import '../models/pass_structure_dictionary.dart';
import 'color_helper.dart';

abstract class MaybeDecode {
  const MaybeDecode._();

  static int? maybeColor(String? colorCode) {
    if (colorCode == null) return null;
    return fromCssColor(colorCode);
  }

  static DateTime? maybeDateTime(String? timeStamp) {
    if (timeStamp == null) return null;
    return DateTime.tryParse(timeStamp);
  }

  static PassTextAlign? maybeTextAlign(String? align) {
    switch (align) {
      case 'PKTextAlignmentLeft':
        return PassTextAlign.left;
      case 'PKTextAlignmentCenter':
        return PassTextAlign.center;
      case 'PKTextAlignmentRight':
        return PassTextAlign.right;
      default:
        return PassTextAlign.natural;
    }
  }

  static PassTextDateStyle? maybeDateStyle(String? style) {
    switch (style) {
      case 'PKDateStyleNone':
        return PassTextDateStyle.None;
      case 'PKDateStyleShort':
        return PassTextDateStyle.Short;
      case 'PKDateStyleMedium':
        return PassTextDateStyle.Medium;
      case 'PKDateStyleLong':
        return PassTextDateStyle.Long;
      case 'PKDateStyleFull':
        return PassTextDateStyle.Full;
      default:
        return null;
    }
  }

  static PassTextNumberStyle? maybeNumberStyle(String? style) {
    switch (style) {
      case 'PKNumberStyleDecimal':
        return PassTextNumberStyle.Decimal;
      case 'PKNumberStylePercent':
        return PassTextNumberStyle.Percent;
      case 'PKNumberStyleScientific':
        return PassTextNumberStyle.Scientific;
      case 'PKNumberStyleSpellOut':
        return PassTextNumberStyle.SpellOut;
      default:
        return null;
    }
  }
}
