/// The PkPass Web Service.
///
/// So far only supports update check but no push service.
///
/// https://developer.apple.com/library/archive/documentation/PassKit/Reference/PassKit_WebService/WebService.html#//apple_ref/doc/uid/TP40011988
library pkpass_web_service;

export 'pkpass_web_wervice/web_service.dart';
export 'pkpass_web_wervice/web_service_error.dart';
