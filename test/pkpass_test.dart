import 'dart:typed_data';

import 'package:archive/archive.dart';
import 'package:intl/locale.dart';
import 'package:test/test.dart';

import 'package:pkpass/pkpass.dart';
import 'package:pkpass/pkpass/utils/file_matcher.dart';

void main() {
  final archive = [
    ArchiveFile('logo.png', 0, Uint8List(0)),
    ArchiveFile('logo@2x.png', 0, Uint8List(0)),
    ArchiveFile('logo@4x.png', 0, Uint8List(0)),
    ArchiveFile('en-us.lproj/logo.png', 0, Uint8List(0)),
    ArchiveFile('en-us.lproj/logo@2x.png', 0, Uint8List(0)),
    ArchiveFile('en-us.lproj/logo@4x.png', 0, Uint8List(0)),
    ArchiveFile('fr.lproj/logo.png', 0, Uint8List(0)),
    ArchiveFile('fr.lproj/logo@2x.png', 0, Uint8List(0)),
    ArchiveFile('fr.lproj/logo@4x.png', 0, Uint8List(0)),
  ];

  // ignore: unused_local_variable
  final file = PassFile(
    PassMetadata(
      description: '',
      formatVersion: 1,
      organizationName: '',
      passTypeIdentifier: '',
      serialNumber: '',
      teamIdentifier: '',
    ),
    archive,
  );
  group('FileMatcher', () {
    test('matchLocale empty locale', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
      );
      expect(localized.length, 3);
      expect(
        localized.every((element) => element.startsWith('logo')),
        isTrue,
      );
    });
    test('matchLocale language fallback', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
        locale: Locale.fromSubtags(languageCode: 'fr', countryCode: 'fr'),
      );
      expect(localized.length, 3);
      expect(
        localized.every((element) => element.startsWith('fr.lproj/')),
        isTrue,
      );
    });
    test('matchLocale region fallback', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
        locale: Locale.fromSubtags(languageCode: 'en', countryCode: 'gb'),
      );
      expect(localized.length, 3);
      expect(
        localized.every((element) => element.startsWith('en-us.lproj/')),
        isTrue,
      );
    });
    test('matchLocale unlnown locale', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
        locale: Locale.fromSubtags(languageCode: 'tlh', countryCode: 'qs'),
      );
      expect(localized.length, 3);
      expect(
        localized.every((element) => element.startsWith('logo')),
        isTrue,
      );
    });
    test('matchScale default', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
      );
      final scaled = FileMatcher.matchScale(
        files: localized,
        name: 'logo',
        extension: 'png',
      );
      expect(scaled, 'logo.png');
    });
    test('matchScale smallest neighbor', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
      );
      final scaled = FileMatcher.matchScale(
        files: localized,
        name: 'logo',
        extension: 'png',
        scale: 6,
      );
      expect(scaled, 'logo@4x.png');
    });
    test('matchScale biggest neighbor', () {
      final localized = FileMatcher.matchLocale(
        files: archive.map((e) => e.name).toList(),
        name: 'logo',
        extension: 'png',
      );
      final scaled = FileMatcher.matchScale(
        files: localized,
        name: 'logo',
        extension: 'png',
        scale: 3,
      );
      expect(scaled, 'logo@4x.png');
    });
  });
}
